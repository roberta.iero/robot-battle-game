import random


def strategy(x_position, y_position, map_x_size, map_y_size, radar_x_pos, radar_y_pos, health):

    if radar_x_pos != 0 and radar_y_pos != 0:
        return 'SHOOT', radar_x_pos, radar_y_pos
    else:
        if health > 80:
            return 'SHOOT', int(map_x_size / random.randint(1, 4)), int(map_y_size / random.randint(1, 4))
        else:
            return 'MOVE', random.randint(1, map_x_size), random.randint(1, map_y_size)