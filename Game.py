import os
import time
import random

import Player
import Map
import Import_Compile

MAX_PLAYERS = 4

MAX_CHALLENGES = 20


# Clear terminal
def clear_terminal():
    os.system('cls' if os.name == 'nt' else 'clear')


# Class Game: initialize the game and contains the main logic
class Game(object):

    def __init__(self):
        # Fixing directory path
        self.game = True

        # Initialize current folder path
        self.file_path = os.path.dirname(os.path.abspath(__file__))
        os.chdir(self.file_path)

        # Create Map instance and getting arena size
        # height = y, width = x
        self.map_instance = Map.Map()
        self.map_height, self.map_width = self.map_instance.getter_sizes()

        # Empty AIs folder from previous match
        file_list = [f for f in os.listdir(self.file_path + '/AIs') if f.endswith('.py')]
        for f in file_list:
            os.remove(os.path.join(self.file_path + '/AIs', f))

        clear_terminal()

        # Welcome msg
        print('Welcome to Robot Battle!')
        time.sleep(0.7)

        print('Looking for Warrior Robots', end='')
        time.sleep(0.3)
        for i in range(1, 4):
            time.sleep(0.4)
            print('.', end='')
        time.sleep(0.4)
        print('\n')

        # Import txt files as .py modules
        Import_Compile.import_compile()

        # Loading AIs as Players
        ai_py_list_total = []
        for AI in os.listdir(self.file_path + '/AIs'):
            name, ext = os.path.splitext(AI)
            if ext == '.py':
                ai_py_list_total.append(AI)

        # Check number of Players
        players_num = len(ai_py_list_total)
        ai_py_list = []
        if players_num == 0:
            print('No Warrior Robots in the ring!')
            exit(0)
        else:
            if players_num == 1:
                print('Ops! We need more Robots to play!')
                exit(0)
            else:
                if players_num > MAX_PLAYERS:
                    print('Ops! We are too many!')
                    time.sleep(1)
                    print('Setting the first match with four Robots.')
                    time.sleep(2)
                    ai_py_list = random.sample(ai_py_list_total, k=4)
                else:
                    time.sleep(1)
                    print('Setting the first match.')
                    time.sleep(2)
                    ai_py_list = ai_py_list_total

        # Create random positions for players
        x_pos = random.sample(range(1, self.map_width + 1), k=4)
        y_pos = random.sample(range(1, self.map_height + 1), k=4)

        # Create Players
        self.robot_list = []
        nickname = 'A'
        pos_index = 0
        for file in ai_py_list:
            name, ext = os.path.splitext(file)
            player = Player.Player(name, nickname, x_pos[pos_index], y_pos[pos_index])
            pos_index = pos_index + 1
            nickname = chr(ord(nickname) + 1)
            self.robot_list.append(player)

        clear_terminal()

        # Initialize the Map
        self.map_instance.initialize_map()

        # Populate the map_instance
        self.map_instance.update_map_with_players(self.robot_list)
        time.sleep(0.5)

        # Initialize the list of allowed action keywords
        self.actions_list = ['MOVE', 'SHOOT', 'STAY']
        self.start_game()

    # Main logic of the Game: each player in turn plays an action and the results of
    # That action are evaluated
    def start_game(self):
        challenge = 0
        # Start game
        while self.game:
            if challenge == MAX_CHALLENGES:
                # Reached max number of turns
                self.game = False
                print('Reached max number of turns!')
                name = ''
                health = 0
                for rob in self.robot_list:
                    if rob.health > health:
                        name = rob.name
                        health = rob.health
                print(name + ': You are the Winner!')
                print_image()
                exit(0)

            else:
                if len(self.robot_list) > 1:
                    challenge = challenge + 1
                    for robot in self.robot_list:
                        print(robot.name + ' is playing: ', end='')
                        time.sleep(0.7)

                        # Radar func() returns the coordinates of an enemy in the 5x5 area or
                        # (0,0) if any
                        radar_x, radar_y = self.map_instance.radar(robot.x_position, robot.y_position)
                        time.sleep(0.4)

                        # Check action format: number of returned parameters == 3, STRING INT INT
                        # If not remove 5 points to the current player and skip turn
                        values = robot.player_action(self.map_width, self.map_height, radar_x, radar_y)

                        if values is not None:
                            # Check number of parameters
                            if len(values) == 3:
                                action = values[0]
                                x = values[1]
                                y = values[2]

                                # Check correct format of arguments
                                if type(action) is str and type(x) is int and type(y) is int:
                                    # Check if the action is valid
                                    if action in self.actions_list:
                                        if action == 'MOVE':
                                            print(action)
                                            time.sleep(1)
                                            self.movement(robot, self.robot_list, x, y)
                                        if action == 'STAY':
                                            print(action)
                                            time.sleep(1)
                                            self.map_instance.update_map_with_players(self.robot_list)
                                        if action == 'SHOOT':
                                            print(action)
                                            time.sleep(1)
                                            self.shoot(robot, self.robot_list, x, y)

                                    else:
                                        # Invalid action
                                        print(robot.name + ' is temporary locked!')
                                        time.sleep(1)
                                        robot.health = robot.health - 5
                                        if robot.health < 0:
                                            robot.health = 0
                                        time.sleep(1)
                                        self.check_health()
                                        self.map_instance.update_map_with_players(self.robot_list)

                                    self.check_health()
                                else:
                                    self.invalid_action(robot)
                            else:
                                self.invalid_action(robot)
                        else:
                            self.invalid_action(robot)
                else:
                    # One robot left: it's the winner
                    self.game = False
                    print(self.robot_list[0].name + ': You are the Winner!')
                    print_image()
                    exit(0)

    # Invalid action: a penalty is applied to the player
    def invalid_action(self, player):
        print('Invalid action: penalty applied!')
        time.sleep(1)
        player.health = player.health - 5
        if player.health < 0:
            player.health = 0
        self.map_instance.update_map_with_players(self.robot_list)

    # Check the health level of each player and update the map/list accordingly
    def check_health(self):
        for elem in self.robot_list:
            if len(self.robot_list) > 1:
                if elem.health <= 0:
                    print(elem.name + ' has left the ring!')
                    time.sleep(1)
                    self.robot_list.remove(elem)
                    self.map_instance.update_map_with_players(self.robot_list)
            else:
                # One robot left: it's the winner
                self.game = False
                print(self.robot_list[0].name + ': You are the Winner!')
                print_image()
                exit(0)

    # Update the health of a player, given the nickname and the penalty to apply
    def update_health(self, nickname, amount):
        for elem in self.robot_list:
            if elem.nickname == nickname:
                elem.health = elem.health - amount
                if elem.health < 0:
                    elem.health = 0

    # Retrieves the player name, given the nickname
    def name_from_nickname(self, nickname):
        for elem in self.robot_list:
            if elem.nickname == nickname:
                return elem.name

    # Action Movement logic
    def movement(self, player, players_list, new_x, new_y):

        # Empty old player position
        time.sleep(0.7)
        player_old_x = player.x_position
        player_old_y = player.y_position
        self.map_instance.set_position_content(player_old_x, player_old_y, ' ')

        x = new_x
        y = new_y

        # height = y, width = x
        # Check if new position is in the map: if not, recompute the position and remove 10 health points
        if not (self.map_width >= new_x > 0 and 0 < new_y <= self.map_height):
            print('Collision with the Wall: ' + player.name + ' looses 10 health points!')
            time.sleep(1)
            player.health = player.health - 10
            if player.health < 0:
                player.health = 0

            if new_x > self.map_width:
                x = self.map_width
                y = new_y
            else:
                if new_y > self.map_height:
                    x = new_x
                    y = self.map_height
                else:
                    if new_x == 0:
                        x = 1
                        if new_y == 0:
                            y = 1
                        else:
                            y = new_y
                    else:
                        x = self.map_width
                        y = self.map_height

        # Check if new position is empty: if not 'Collision with another Robot', both loose 15 points
        content_position = self.map_instance.get_position_content(x, y)
        if content_position != ' ' and content_position != player.nickname:
            player.health = player.health - 15
            if player.health < 0:
                player.health = 0

            print('Collision with player ' + content_position + ': both loose 15 health points!')
            time.sleep(1)
            self.update_health(content_position, 15)
            self.check_health()

            # Look for the nearest empty cell in the map
            empty_x, empty_y = self.map_instance.look_for_empty_cell(x, y)
            player.x_position = empty_x
            player.y_position = empty_y
        else:
            player.x_position = x
            player.y_position = y

        time.sleep(1)

        # Updates the map and ends the current turn
        self.map_instance.update_map_with_players(players_list)

    def shoot(self, player, players_list, target_x, target_y):

        # Check target cell is in the map
        is_in_map = self.map_instance.is_in_the_map(target_x, target_y)
        if is_in_map:
            # Check the field is not the player current position
            if player.x_position == target_x and player.y_position == target_y:
                print('Failed to shoot: ' + player.name + 'almost shot itself!')
                time.sleep(1)
                player.health = player.health - 5
                self.check_health()
                self.map_instance.update_map_with_players(self.robot_list)
            else:
                # Check the target field is not empty
                content_position = self.map_instance.get_position_content(target_x, target_y)
                if content_position != ' ':
                    target_name = self.name_from_nickname(content_position)
                    self.map_instance.update_map_with_shot(self.robot_list, target_x, target_y)

                    print('Robot ' + target_name + ' has been attacked!')
                    time.sleep(1)
                    self.update_health(content_position, 50)
                    self.map_instance.update_map_with_players(self.robot_list)

                else:
                    self.map_instance.update_map_with_shot(self.robot_list, target_x, target_y)
                    print('No enemy in the target position: all the Robots are safe.')
                    time.sleep(1)
                    self.map_instance.update_map_with_players(self.robot_list)
                    time.sleep(1)
        else:
            print('Target outside the ring: all the Robots are safe.')
            time.sleep(1)
            self.map_instance.update_map_with_players(self.robot_list)
            time.sleep(1)


def print_image():
    print('''        
                                ()         \ \//                    
                              __||__       \ _ /
                             |     |       //
                             | | | |      //
                             |_____|    (__)
                       ____ ___|_|___ __//
                      ()___)         ()___)
                      //  |           |
                    (__)  |___________| 
                     ||     (_______)
                     ||        |_|      
                    /__\   ___/___\___   
                    ||||  |           |  
                          |___________| 
                           | |     | |
                           | |     | |
                         __| |_   _| |__
                        |______| |______|  




                ''')


def main():
    Game()


if __name__ == '__main__':
    main()
