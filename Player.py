import importlib


# Class Player contains the player attributes
# and dynamically link to a player it's strategy loaded from text file
class Player(object):
    name = ""
    nickname = ""
    alive = True
    health = 100
    x_position = 0
    y_position = 0

    def __init__(self, name, nickname, x_position, y_position):
        self.name = name
        self.nickname = nickname
        self.x_position = x_position
        self.y_position = y_position
        self.module = importlib.import_module('.' + self.name, package='AIs')

    def player_action(self, map_x_size, map_y_size, radar_x_pos, radar_y_pos):
        return self.module.strategy(self.x_position, self.y_position, map_x_size, map_y_size, radar_x_pos, radar_y_pos,
                                    self.health)
