import random


def strategy(x_position, y_position, map_x_size, map_y_size, radar_x_pos, radar_y_pos, health):

    if radar_x_pos != 0 and radar_y_pos != 0:
        return 'SHOOT', radar_x_pos, radar_y_pos
    else:
        if health >10:
            return 'MOVE', random.randint(1, map_x_size), random.randint(1, map_x_size)
        else:
            return 'SHOOT', x_position + 6, y_position + 6