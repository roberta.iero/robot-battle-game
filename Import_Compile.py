import ast
import os


# Import the text files and store them as python modules, if they contains valid code
def import_compile():
    file_path = os.path.dirname(os.path.abspath(__file__))
    os.chdir(file_path)

    for txt in os.listdir(file_path + '/FILE_TXT'):
        file = open('FILE_TXT/' + txt, 'r')
        content = file.read()

        name, ext = os.path.splitext(txt)

        if is_valid_py(txt, content):
            file_py = open('AIs/' + name + '.py', 'x')
            file_py.close()

            file_py = open('AIs/' + name + '.py', 'w')
            file_py.write(content)
            file_py.close()

        file.close()


# Check the imported text file contains valid python code
def is_valid_py(f_name, content):
    try:
        compile(content, f_name, 'exec')
        return True
    except SyntaxError:
        print(f_name, ' is not a valid py code!')
        return False
