import random

def strategy(x_position, y_position, map_x_size, map_y_size, radar_x_pos, radar_y_pos, health):

    random_x = random.randint(1, map_x_size)
    random_y = random.randint(1, map_y_size)

    if radar_x_pos != 0 and radar_y_pos != 0:
        return 'SHOOT', radar_x_pos, radar_y_pos
    else:
        if health <= 10:
            return 'STAY', x_position, y_position
        else:
            return 'MOVE', random_x, random_y
