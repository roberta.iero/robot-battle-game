import os
import time


# Class Map: defines/updates the Map and the Status block for log on game status
class Map(object):
    field = {1: []}
    status = {1: []}

    # y
    WIDTH = 14
    # x
    HEIGHT = 8

    def __init__(self):
        self.field = [
            ['+', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '+'],
            ['|', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '|'],
            ['|', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '|'],
            ['|', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '|'],
            ['|', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '|'],
            ['|', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '|'],
            ['|', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '|'],
            ['|', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '|'],
            ['|', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '|'],
            ['+', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '+']
        ]

        self.status = [
            ['Initializing game'],
            ['___________________________'],
            [''],
            [''],
            [''],
            [''],
            ['___________________________'],
        ]

    # Print the Map and the status block at the beginning of the game
    def initialize_map(self):
        print('\n'.join(map('  '.join, self.field)))
        print('\n'.join(map(''.join, self.status)))
        time.sleep(1)

    # Print the Map and the status block during the game
    def print_map(self):
        print('\n'.join(map('  '.join, self.field)))
        print('\n')
        print('\n'.join(map(''.join, self.status)))
        print('\n')

    # Clear the Map and Status block
    def clear_status(self):
        self.status = [
            ['PLAYERS'],
            ['___________________________'],
            [''],
            [''],
            [''],
            [''],
            ['___________________________'],
        ]

        self.field = [
            ['+', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '+'],
            ['|', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '|'],
            ['|', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '|'],
            ['|', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '|'],
            ['|', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '|'],
            ['|', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '|'],
            ['|', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '|'],
            ['|', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '|'],
            ['|', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '|'],
            ['+', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '+']
        ]

    # Update the Map inserting the Players in their position
    # and their information in the status block
    def update_map_with_players(self, players_list):
        self.clear_status()

        i = 2
        for player in players_list:
            x = player.x_position
            y = player.y_position

            self.field[x][y] = player.nickname
            self.status[0] = 'Players:'
            self.status[i] = (player.nickname + '.  ' + player.name + '  Health: ' + str(player.health))
            i = i + 1

        # Clear terminal
        os.system('cls' if os.name == 'nt' else 'clear')

        self.print_map()

    # Update the map to visualize with an X the target cell of a missiles
    def update_map_with_shot(self, players_list, x_target, y_target):
        self.clear_status()

        i = 2
        for player in players_list:
            x = player.x_position
            y = player.y_position

            self.field[x][y] = player.nickname
            self.status[0] = 'Players:'
            self.status[i] = (player.nickname + '.  ' + player.name + '  Health: ' + str(player.health))
            i = i + 1

        self.field[x_target][y_target] = 'X'

        # Clear terminal
        os.system('cls' if os.name == 'nt' else 'clear')

        self.print_map()

    # Returns the map sizes
    def getter_sizes(self):
        return self.WIDTH, self.HEIGHT

    # Returns the content of the x,y coordinate in the map
    def get_position_content(self, x, y):
        return self.field[x][y]

    # Sets the content of the x,y coordinate in the map
    def set_position_content(self, x, y, content):
        self.field[x][y] = content

    # Flags if the x,y coordinate is in the map
    def is_in_the_map(self, x, y):
        if self.HEIGHT >= x > 0:
            if 0 < y <= self.WIDTH:
                return True
            else:
                return False
        else:
            return False

        # y
        # WIDTH = 14
        # x
        # HEIGHT = 8

    # Looks for an empty cell nearby the one with the x,y coordinates
    def look_for_empty_cell(self, x, y):
        x_start = x - 1
        y_start = y - 1

        for i in range(x_start, x_start + 3):
            for j in range(y_start, y_start + 3):
                if self.is_in_the_map(i, j):
                    if self.get_position_content(i, j) == ' ':
                        return i, j

    # Looks for an enemy nearby the x,y coordinate of the current player. It returns 0,0 if none
    def radar(self, x, y):
        x_start = x - 1
        y_start = y - 1

        # Check 3x3 area
        for i in range(x_start, x_start + 3):
            for j in range(y_start, y_start + 3):
                if self.is_in_the_map(i, j):
                    if self.get_position_content(i, j) != ' ':
                        # Same y different x
                        if i != x:
                            if j == y:
                                return i, j
                        # Same x different y
                        if j != y:
                            if i == x:
                                return i, j
                        # Different x,y
                        if j != y:
                            if i != x:
                                return i, j

        x_start = x - 2
        y_start = y - 2

        x_coordinate, y_coordinate = self.row_cycle(x_start, y_start)

        if x_coordinate != 0 and y_coordinate != 0:
            return x_coordinate, y_coordinate
        else:
            x_coordinate, y_coordinate = self.row_cycle(x_start + 4, y_start)
            if x_coordinate != 0 and y_coordinate != 0:
                return x_coordinate, y_coordinate
            else:
                x_coordinate, y_coordinate = self.column_cycle(x_start, y_start)
                if x_coordinate != 0 and y_coordinate != 0:
                    return x_coordinate, y_coordinate
                else:
                    x_coordinate, y_coordinate = self.column_cycle(x_start, y_start + 4)
                    if x_coordinate != 0 and y_coordinate != 0:
                        return x_coordinate, y_coordinate
        return 0, 0

    # Cycle on row from a starting point to look for an enemy in the map
    def row_cycle(self, x_start, y_start):
        for k in range(y_start, y_start + 4):
            if self.is_in_the_map(x_start, k):
                if self.get_position_content(x_start, k) != ' ':
                    return x_start, k
        return 0, 0

    # Cycle on column from a starting point to look for ana enemy in the map
    def column_cycle(self, x_start, y_start):
        for l in range(x_start, x_start + 4):
            if self.is_in_the_map(l, y_start):
                if self.get_position_content(l, y_start) != ' ':
                    return l, y_start
        return 0, 0
