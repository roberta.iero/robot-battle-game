# Robot Battle Game

### Robot Battle Game is a simple Programming Game.



## Game Description

### Overview

Robot Battle Game is a game based on computer programming. The players' strategies must be defined in advance by Users, before the actual game begins.
A player's strategy consists in a text input file, that contains Python code. The mission of each Robot is to compete and destroy the other Robots in the ring. Up to four Robots can compete at once and only one can be declared Winner.


Robot Battle Game imports the Users' strategies from the text input files and compile their contents, in orther to load, as a Python module, only the ones containing valuable Python code.
For each loaded Python module, a Robot player is created (up to a maximum of 4). The Robots are allowed to move/shoot in turn in order to compete with the others. The match ends either when there is one Robot left in the ring or when the maximum number of turns has been reached.

### User Interface
The game is displayed in a Command Line interface. Robots inside the arena, their current status information and action logs are shown on the screen.

### Requirements
In order to play the Game, Python 3.8.0 is required.


## Rules of the Game
The game starts with a minimum of 2 Robots to a maximum of 4. Each Robot is placed in a random position inside the Arena, which is a matrix with fixed dimensions.
Each player in turn can play one action: MOVE, SHOOT, STAY and the consequences of the actions are analyzed and reflected on the game status and on the screen.

When calling the Robot strategy, the returned action syntax and format are checked: if not valid the Robot will receive a penalty of -5 health points.

### Radar
Each turn a Robot receives a radar information along with its current position and its current health.
*  If in the 5x5 area, with center in the Robot current position, there is another Robot, the exact coordinates of that Robot will be sent to the one playing the current turn
*  Otherwise, the radar will send a (0,0) coordinate.

### Move:
The Robot choose an (x,y) coordinate for its movement:
* If the (x,y) is outside the Arena, the Robot collides with the Wall and looses 10 health points.
* If the (x,y) is in the Arena but there is another Robot in it, the two Robots collides and both loose 15 health points.The Robot playing the turn will be positioned in the first empty position near the target one for the movement.
* If the (x,y) is in the Arena and it is empty, the Robot is moved into the new position.


### Stay
The Robot does nothing and remains in the current position in the Arena.

### Shoot
The Robot choose an (x,y) target coordinate to shoot:
* If the target (x,y) is empty or outside the Arena, no Robot is affected and the current turn ends.
* If the target (x,y) is the the current position of the Robot playing the turn, the Robot will receive a penalty of -5 health points.
* If the tarfet (x,y) is not empty and inside the Arena, the Robot hit by the missiles looses 50 health points.


At the end of each turn, if there is only one Robot left in the ring it will be declared Winner. Otherwise the game continues until the maximum number of challenges (a challenge is intended as a complete turn in which each Robot has played its action) has been reached.


## Designing Robots Strategies
The Robot strategies consists of a text input file (.txt) containing a valid Python code. The code should respect the following structure:

**def strategy(x_position, y_position, map_x_size, map_y_size, radar_x_pos, radar_y_pos, health):**

   
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;...
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;...
   
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return 'ACTION_KEYWORD', X_COORDINATE, Y_COORDINATE


The parameters received from the game are:
*  **x_position:** current x coordinate of the Robot
*  **y_position:** current y coordinate of the Robot
*  **map_x_size:** maximum x coordinate of the Arena (minimum 1)
*  **map_y_size:** maximum y coordinate of the Arena (minimum 1)
*  **health:** current Robot health points

The return statement should contain:
*  **Action Keyword:** MOVE, STAY, SHOOT as a str 
*  **X coordinate:** int value of x (it represents the x coordinate where to MOVE or SHOOT)
*  **Y coordinate:** int value of y (it represents the y coordinate where to MOVE or SHOOT)

If the specified format is not respected, the action will be considered invalid. The (x,y) specified in the action STAY are ignored.

This project comes with few exemples of Robot Strategies to take inspiration and to test the Game. If you want to add/replace the existing Robot Strategies, please look at the FILE_TXT folder: delete/modify the existing ones or save new ones with .txt extension.


## Running Instructions
1.  Clone Git project: 

    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; `git clone git@gitlab.com:roberta.iero/robot-battle-game.git`  or  `git clone https://gitlab.com/roberta.iero/robot-battle-game.git`
    
2.  Check Python version (3.8.0 needed)

3.  Execute the Game.py module: 

    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`python3 Game.py`


## License
This project is licensed under the MIT License - see the LICENSE.md file for details

