import random

def strategy(x_position, y_position, map_x_size, map_y_size, radar_x_pos, radar_y_pos, health):
    if radar_x_pos != 0:
        return 'SHOOT', radar_x_pos, radar_y_pos
    else:
        if health > 30:
            x = x_position + random.randint(-1,1)
            y = y_position + random.randint(-1,1)
            return 'MOVE', x, y

        else:
            return 'SHOOT', int(x_position +2), int(y_position +2)

